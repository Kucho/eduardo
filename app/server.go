package app

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/Kucho/eduardo/config"
)

var (
	configFile string
)

// StartDefaultServer inicia el servidor por defecto
func StartDefaultServer() {

	// Carga la configuración
	conf, err := config.LoadConf(configFile)
	if err != nil {
		log.Panic(err)
	}

	// Empieza la conexión inicial con la base de datos
	s := NewStorage(conf)
	err = s.InitDB()
	if err != nil {
		log.Panic(err)
	}

	r := chi.NewRouter()

	// Base de middlewares
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	// Pone un valor timeout en el request context (ctx), que avisará a través de ctx.Done()
	// que la request ha caducado y cualquier procesamiento posterior debe detenerse
	r.Use(middleware.Timeout(120 * time.Second))

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Greetings from the API"))
	})

	// Montar el api subrouter
	r.Mount("/api", s.apiRouter())

	// Montar el admin subrouter
	r.Mount("/admin", s.adminRouter())

	fmt.Printf("Now serving API on http://localhost:%s\n", conf.Core.Port)
	http.ListenAndServe(":"+conf.Core.Port, r)

}

// Un router completamente diferente para las APIS
func (s *Storage) apiRouter() chi.Router {
	r := chi.NewRouter()
	r.Use(AdminOnly)

	r.Route("/{type}", func(r chi.Router) {
		r.Use(AdminOnly)
		r.Post("/", s.Add)
		r.Get("/", s.GetAll)

		r.Route("/{key}", func(r chi.Router) {
			r.Get("/", s.GetKey)
			r.Patch("/", s.Update)
			r.Delete("/", s.Delete)

			r.Route("/{value}", func(r chi.Router) {
				r.Get("/", s.GetKey)
				r.Patch("/", s.Update)
				r.Delete("/", s.Delete)
			})
		})
	})
	return r
}

// Un router completamente diferente para los administradores
func (s *Storage) adminRouter() chi.Router {
	r := chi.NewRouter()
	r.Use(AdminOnly)
	r.Get("/", AdminDB)
	r.Get("/profile", AdminInfo)

	/*
		r.Get("/users/{userId}", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(fmt.Sprintf("admin: view user id %v", chi.URLParam(r, "userId"))))
		})*/
	return r
}

// AdminOnly es un middleware que restringe el acceso solo a administradores
func AdminOnly(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		/*isAdmin, ok := r.Context().Value("acl.admin").(bool)
			if !ok || !isAdmin {
			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}*/
		next.ServeHTTP(w, r)
	})
}
