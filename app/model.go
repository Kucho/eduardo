package app

import (
	"time"
)

// RequiredField devuelve los campos que se requieren obligatoriamente
var RequiredField = make(map[string][]string)

// ReferencedCols Collection Foreign Keys Fields... One to many
var ReferencedCols = make(map[string][]ReferencedKey)

func init() {
	// Campos requeridos por cada modelo
	RequiredField["Boooking"] = []string{"code", "date", "state", "price"}
	RequiredField["Cargo"] = []string{"code"}
	RequiredField["CargoType"] = []string{"code"}
	RequiredField["Client"] = []string{"code"}
	RequiredField["DocType"] = []string{"code"}
	RequiredField["Company"] = []string{"code"}
	RequiredField["Outgoing"] = []string{"code"}
	RequiredField["Passenger"] = []string{"code"}
	RequiredField["Route"] = []string{"code"}
	RequiredField["Staff"] = []string{"code"}
	RequiredField["Ship"] = []string{"code"}
	RequiredField["Transaction"] = []string{"code"}

	// Se inicializa las relaciones entre los elementos de cada modelo

	// Celdas referenciadas de Booking
	ReferencedCols["Booking"] = append(ReferencedCols["Booking"], createRefCol("Booking", "passengers", "Passenger", "_id"))
	ReferencedCols["Booking"] = append(ReferencedCols["Booking"], createRefCol("Booking", "cargos", "Cargo", "_id"))

	// Celdas referenciadas de Cargo
	ReferencedCols["Cargo"] = append(ReferencedCols["Cargo"], createRefCol("Cargo", "route", "Route", "_id"))
	ReferencedCols["Cargo"] = append(ReferencedCols["Cargo"], createRefCol("Cargo", "sender", "Passenger", "_id"))
	ReferencedCols["Cargo"] = append(ReferencedCols["Cargo"], createRefCol("Cargo", "type", "CargoType", "_id"))

	// Celdas referenciadas de Client
	ReferencedCols["Client"] = append(ReferencedCols["Client"], createRefCol("Client", "passenger", "Passenger", "_id"))

	// Celdas referenciadas de Outgoing
	ReferencedCols["Outgoing"] = append(ReferencedCols["Outgoing"], createRefCol("Outgoing", "trip", "Trip", "_id"))

	// Celdas referenciadas de Passenger
	ReferencedCols["Passenger"] = append(ReferencedCols["Passenger"], createRefCol("Passenger", "doctype", "DocType", "_id"))

	// Celdas referenciadas de Transaction
	ReferencedCols["Transaction"] = append(ReferencedCols["Transaction"], createRefCol("Transaction", "booking", "Booking", "_id"))
	ReferencedCols["Transaction"] = append(ReferencedCols["Transaction"], createRefCol("Transaction", "company", "Company", "_id"))
	ReferencedCols["Transaction"] = append(ReferencedCols["Transaction"], createRefCol("Transaction", "seller", "Staff", "_id"))

	// Celdas referenciadas Trip
	ReferencedCols["Trip"] = append(ReferencedCols["Trip"], createRefCol("Trip", "route", "Route", "_id"))
	ReferencedCols["Trip"] = append(ReferencedCols["Trip"], createRefCol("Trip", "ship", "Ship", "_id"))

}

// createRefCol constructor
func createRefCol(name, sfield, end, efield string) ReferencedKey {
	rc := ReferencedKey{Start: name, SField: sfield, End: end, EField: efield}
	return rc
}

// ReferencedKey es la estructura de las llaves referenciadas
type ReferencedKey struct {
	// Colección de origen
	Start string
	// Field de la colección de origen
	SField string
	// Colección de destino
	End string
	// Field de la colección de destino
	EField string
}

// DBDetails es la estructura del id-key-rev de un documento en la base de datos
type DBDetails struct {
	ID  string `json:"_id"`
	Key string `json:"_key"`
	Rev string `json:"_rev"`
}

// Booking es la estructura de una reserva
// EDGE
type Booking struct {
	DBDetails
	Code       string    `json:"code"`
	Passengers []string  `json:"passengers"` // ID de los pasajeros
	Cargos     []string  `json:"cargos"`     // ID de los cargos
	Price      float64   `json:"price"`
	State      string    `json:"state"` // En espera - Pagado - Expirado - Cancelado
	Date       time.Time `json:"date"`  // Hora de reserva
}

// BookingCol es una colección de reservas
type BookingCol struct {
	Bookings []Booking `json:"bookings"`
}

// Cargo es la estructura de una encomienda
type Cargo struct {
	DBDetails
	Code       string `json:"code"`
	Route      string `json:"route"`  // ID de la ruta
	State      string `json:"state"`  // En espera - En camino - Entregado
	Sender     string `json:"sender"` // ID del cliente que envía mercadería
	Reciever   string `json:"reciever"`
	RecieverID int    `json:"recieverid"` // Número de documento del que recibe
	Type       string `json:"type"`       // ID del tipo de mercadería
	High       int    `json:"high"`
	Wide       int    `json:"wide"`
	Long       int    `json:"long"`
	Obs        string `json:"obs"`
}

// CargoCol es una colección de Cargo
type CargoCol struct {
	Cargos []Cargo `json:"cargos"`
}

// CargoType es la estructura de un tipo de carga y contiene el precio
type CargoType struct {
	DBDetails
	Code        string  `json:"code"`
	Description string  `json:"description"`
	Price       float64 `json:"price"`
}

// CargoTypeCol es una colección de tipo de cargos (CargoType)
type CargoTypeCol struct {
	CargoTypes []CargoType `json:"cargotypes"`
}

// Client es la estructura de un cliente registrado en la página
type Client struct {
	DBDetails
	Code         string `json:"code"`
	User         string `json:"user"`
	PasswordHash []byte `json:"pwh"`
	Email        string `json:"email"`
	Passennger   string `json:"passenger"` // ID del pasajero
}

// ClientCol es una colección de clientes
type ClientCol struct {
	Clients []Client `json:"clients"`
}

// DocType es la estructura del tipo de documento
type DocType struct {
	DBDetails
	Code      string `json:"Code"`
	Name      string `json:"name"`
	Shortname string `json:"shortname"`
}

// DocTypeCol es la colección de los tipos de documento
type DocTypeCol struct {
	DocTypes []DocType `json:"doctypes"`
}

// Company es la información de una empresa para la facturación
type Company struct {
	DBDetails
	Code    string `json:"code"`
	RUC     int    `json:"ruc"`
	Name    string `json:"name"`
	Address string `json:"address"`
	City    string `json:"city"`
}

// CompanyCol es una colección de empresas
type CompanyCol struct {
	Companies []Company `json:"companies"`
}

// Outgoing es la estructura de un gasto de un viaje
type Outgoing struct {
	DBDetails
	Code        string  `json:"Code"`
	Trip        string  `json:"trip"` // ID del trip
	Description string  `json:"descripcion"`
	Quantity    int     `json:"quantity"`
	Price       float64 `json:"price"`
}

// OutgoingCol es una colección de gastos
type OutgoingCol struct {
	Outgoings []Outgoing `json:"outgoings"`
}

// Passenger es la estructura de los pasajeros
type Passenger struct {
	DBDetails
	Code       int       `json:"code"`    // Código de País + Tipo de documento + Número de documento | Ejemplo: (51) (1) (71088058) -> 51171088058
	DocType    string    `json:"doctype"` // ID del tipo de documento
	DocNumber  int       `json:"docnumber"`
	FirstNames string    `json:"firstnames"`
	LastNames  string    `json:"lastnames"`
	Sex        string    `json:"sex"`
	BirthDate  time.Time `json:"birthday"`
	Email      string    `json:"email"`
	Phone      string    `json:"phone"`
	Country    string    `json:"country"`
	EName      string    `json:"ename"`
	ENumber    string    `json:"enumber"`
}

// PassengerCol es una colección de pasajeros
type PassengerCol struct {
	Passengers []Passenger `json:"passengers"`
}

// Route es la estructura de una ruta de viaje
type Route struct {
	DBDetails
	Code        string  `json:"code"`
	Origin      string  `json:"origin"`
	Destination string  `json:"destination"`
	Price       float64 `json:"price"`
}

// RouteCol es una colección de rutas de viaje
type RouteCol struct {
	Routes []Route `json:"routes"`
}

// Staff es la estructura de un colaborador
type Staff struct {
	DBDetails
	Code         string   `json:"code"`
	Username     string   `json:"username"`
	PasswordHash []byte   `json:"pwh"`
	FirstNames   string   `json:"firstnames"`
	LastNames    string   `json:"lastnames"`
	Email        string   `json:"email"`
	CanSee       []string `json:"csee"`    // File (or table) of the DB
	CanEdit      []string `json:"cedit"`   // File (or table) of the DB
	CanDelete    []string `json:"cdelete"` // File (or table) of the DB
}

// StaffCol es una colección de colaboradores
type StaffCol struct {
	Staffs []Staff `json:"staffs"`
}

// Ship es la estructura de una nave
type Ship struct {
	DBDetails
	Code     string `json:"code"`
	Name     string `json:"name"`
	State    string `json:"state"`
	Capacity int    `json:"capacity"`
}

// ShipCol es una colección de naves
type ShipCol struct {
	Ships []Ship `json:"ships"`
}

// Transaction es la estructura de una operación referente a una reserva
// EDGE
type Transaction struct {
	DBDetails
	Code    string    `json:"code"`
	Booking string    `json:"booking"` // ID del booking
	Company string    `json:"company"` // ID de la empresa
	Total   float64   `json:"total"`
	Date    time.Time `json:"date"`    // Hora de la transacción
	DocType string    `json:"doctype"` // Factura electrónica, Boleta, Nota de crédito, Nota de débito
	DocID   string    `json:"docid"`
	State   string    `json:"state"`  // Éxito - Cancelado - Error
	Seller  string    `json:"seller"` // ID del vendedor
}

// TransactionCol es una colección de transacciones
type TransactionCol struct {
	Transactions []Transaction `json:"transactions"`
}

// Trip es la estructura de un viaje por nave, en una ruta y fecha determinada
// EDGE
type Trip struct {
	DBDetails
	Code  string    `json:"code"`
	State string    `json:"state"`
	Date  time.Time `json:"date"`
	Route string    `json:"route"` // ID de la ruta
	Ship  string    `json:"ship"`  // ID de la nave
	Count int       `json:"count"`
}

// TripCol es una colección de viajes
type TripCol struct {
	Trips []Trip `json:"trips"`
}
