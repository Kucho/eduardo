package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"time"

	driver "github.com/arangodb/go-driver"
	ahttp "github.com/arangodb/go-driver/http"
	"gitlab.com/Kucho/eduardo/config"
)

var (
	ctx context.Context
	// COLLS es la lista de todas las colecciones de la base de datos
	COLLS = []string{"Passenger", "Ship", "Route", "Cargo", "CargoType", "DocType", "Staff", "Company", "Outgoing", "Client"}
	// EDGES son las colecciones edge
	EDGES = []string{"Transaction", "Trip", "Booking"}
)

// Storage es la estructura que cuenta con todos los elemtos de la base de datos
type Storage struct {
	config config.ConfYaml
	conn   driver.Connection
	client driver.Client
	db     driver.Database
	err    error
}

// NewStorage inicializa una instancia Storage con la confiración brindada
func NewStorage(config config.ConfYaml) *Storage {
	return &Storage{
		config: config,
	}
}

// InitDB inicializa la conexión con la base de datos para verificar que todo esté en orden.
// Asimismo, comprueba la base de datos y las colecciones iniciales.
func (s *Storage) InitDB() error {

	host := s.config.Database.ArangoDB.Host
	port := s.config.Database.ArangoDB.Port
	user := s.config.Database.ArangoDB.User
	pwd := s.config.Database.ArangoDB.Password

	s.conn, s.err = ahttp.NewConnection(ahttp.ConnectionConfig{
		Endpoints: []string{"http://" + host + ":" + port},
	})
	if s.err != nil {
		fmt.Printf("ArangoDB: Connection to %s:%s failed", host, port)
		return s.err
	}

	s.client, s.err = driver.NewClient(driver.ClientConfig{
		Connection:     s.conn,
		Authentication: driver.BasicAuthentication(user, pwd),
	})
	if s.err != nil {
		fmt.Println("ArangoDB: Authentication to database failed")
		return s.err
	}

	s.waitUntilServerAvailable()
	fmt.Printf("ArangoDB: Connected to http://%s:%s\n", ""+host, port)
	s.CheckDatabase()
	s.CheckCollections()
	return nil
}

// waitUntilServerAvailable queda en espera hasta que el servidor/cluster que el cliente trata de accedder esté disponible
func (s *Storage) waitUntilServerAvailable() bool {
	instanceUp := make(chan bool)
	ctx = context.Background()
	go func() {
		for {
			verCtx, cancel := context.WithTimeout(ctx, time.Second*5)
			if _, s.err = s.client.Version(verCtx); s.err != nil {
				cancel()
				time.Sleep(time.Second)
				fmt.Printf("Connection to http://%s:%s failed, re-trying in 5 seconds\n", s.config.Database.ArangoDB.Host, s.config.Database.ArangoDB.Port)
			} else {
				cancel()
				instanceUp <- true
				return
			}

		}
	}()
	select {
	case up := <-instanceUp:
		return up
	case <-ctx.Done():
		return false
	}
}

func (s *Storage) ensureIndexes(collection string, attributes []string) {
	var col driver.Collection

	col, s.err = s.db.Collection(ctx, collection)
	if s.err != nil {
		log.Panic(s.err)
	}

	opt := driver.EnsurePersistentIndexOptions{Unique: false, Sparse: false}
	col.EnsurePersistentIndex(ctx, attributes, &opt)
}

// CheckCollections chequea que las colecciones por defectos existan. Si no, las crea.
func (s *Storage) CheckCollections() {
	// Abre la base de datos
	s.db, _ = s.client.Database(ctx, s.config.Database.ArangoDB.DatabaseName)
	opt := &driver.CreateCollectionOptions{ /* ... */ }
	go func() {
		for i := 0; i < len(COLLS); i++ {
			_, s.err = s.db.CreateCollection(ctx, COLLS[i], opt)
			if s.err == nil {
				fmt.Printf("ArangoDB: Collection '%s' created \n", COLLS[i])
			}
			// Nos aseguramos de indexear en disco los atributos
			s.ensureIndexes(COLLS[i], []string{"code"})
		}
	}()

	opt2 := &driver.CreateCollectionOptions{IsSmart: true, Type: driver.CollectionTypeEdge}
	go func() {
		for i := 0; i < len(EDGES); i++ {
			_, s.err = s.db.CreateCollection(ctx, EDGES[i], opt2)
			if s.err == nil {
				fmt.Printf("ArangoDB: Edge collection '%s' created \n", EDGES[i])
			}
			// Nos aseguramos de indexear en disco los atributos
			s.ensureIndexes(EDGES[i], []string{"code"})
		}
	}()
}

// CheckDatabase chequea que la base de datos exista. Si no, la crea.
func (s *Storage) CheckDatabase() {
	dbname := s.config.Database.ArangoDB.DatabaseName
	options := &driver.CreateDatabaseOptions{ /* ... */ }
	_, s.err = s.client.CreateDatabase(ctx, dbname, options)
	if s.err == nil {
		fmt.Printf("ArangoDB: Database '%s' did not exist, creating new one \n", dbname)
	}
	fmt.Println("ArangoDB: Database ready")
}

// NewDoc crea un nuevo documento en la colección definida
func (s *Storage) NewDoc(collection string, doc interface{}) (interface{}, int) {
	var col driver.Collection

	col, s.err = s.db.Collection(ctx, collection)
	if s.err != nil {
		//	fmt.Printf("Failed to open collection '%s': %s", collection, s.err)
		return nil, http.StatusInternalServerError
	}

	var meta driver.DocumentMeta
	meta, s.err = col.CreateDocument(ctx, doc)
	if s.err != nil {
		return nil, http.StatusInternalServerError
	}

	// Verifica los datos en la DB y extrae con Key
	rn, _ := s.ReadDocs(collection, "_key", meta.Key)
	return rn, http.StatusCreated
}

// UpdateDoc actualliza un (01) documento que tenga cumpla con ell key = value
func (s *Storage) UpdateDoc(collection, key string, value, doc interface{}) (interface{}, int) {
	var col driver.Collection

	col, s.err = s.db.Collection(ctx, collection)
	if s.err != nil {
		//fmt.Printf("ArangoDB: Failed to open collection '%s': %s", collection, s.err)
		return nil, http.StatusInternalServerError
	}

	// Tenemos que asegurarnos que el documento que vamos a actualizar es único.
	r, _ := s.ReadDocs(collection, key, value)

	if len(r) == 0 {
		//	fmt.Println("ArangoDB: Target document not found")
		return nil, http.StatusNotFound
	}

	if len(r) > 1 {
		//fmt.Println("ArangoDB: Document is not unique")
		return nil, http.StatusBadRequest
	}

	var v reflect.Value
	var vk string

	if len(r) == 1 {
		v = reflect.ValueOf(r[0]).Elem()
		rk := reflect.Indirect(v).FieldByName("Key")
		vk = rk.String()
		_, s.err = col.UpdateDocument(ctx, vk, doc)
		if s.err != nil {
			//		log.Panic(s.err)
			return nil, http.StatusInternalServerError
		}
	}

	rd, _ := s.ReadDocs(collection, key, vk)
	return rd, http.StatusOK
}

// ReadDocs retorna los documentos encontrados en un slice, así como las keys en otro
func (s *Storage) ReadDocs(collection, key string, value interface{}) ([]interface{}, int) {

	bindVars := make(map[string]interface{})
	ctx := driver.WithQueryCount(context.Background())

	filter := " "
	// Si el valor pasado es vacío, entones no ejecutamos ningún filto, retornamos toda la colecciòón
	if !IsZero(value) {
		filter = " FILTER d.@key == @value "
		bindVars["key"] = key
		bindVars["value"] = value
	}

	query := "FOR d IN " + collection + filter + "RETURN d"

	var cursor driver.Cursor
	cursor, s.err = s.db.Query(ctx, query, bindVars)
	if s.err != nil {
		return nil, http.StatusInternalServerError
	}
	defer cursor.Close()

	if cursor.Count() == 0 {
		return nil, http.StatusNotFound
	}

	var slice []interface{}
	var keys []string

	for i := 0; i < int(cursor.Count()); i++ {
		var meta driver.DocumentMeta
		found := s.NewModel(collection)
		meta, s.err = cursor.ReadDocument(ctx, &found)
		if s.err != nil {
		}
		slice = append(slice, found)
		keys = append(keys, meta.Key)
	}
	return slice, http.StatusOK
}

//RemoveDoc elimina un (01) documento único de una colección que cumpla con el key=value
func (s *Storage) RemoveDoc(collection, key string, value interface{}) (interface{}, int) {

	var col driver.Collection

	col, s.err = s.db.Collection(ctx, collection)
	if s.err != nil {
		//	fmt.Printf("ArangoDB: Failed to open collection '%s': %s", collection, s.err)
		return nil, http.StatusInternalServerError
	}

	r, _ := s.ReadDocs(collection, key, value)

	if len(r) == 0 {
		//fmt.Println("ArangoDB: Target document not found")
		return nil, http.StatusNotFound
	}

	if len(r) > 1 {
		//	fmt.Println("ArangoDB: Document is not unique")
		return nil, http.StatusBadRequest
	}

	var v reflect.Value
	var vk string

	if len(r) == 1 {
		v = reflect.ValueOf(r[0]).Elem()
		rk := reflect.Indirect(v).FieldByName("Key")
		vk = rk.String()
		_, s.err = col.RemoveDocument(ctx, vk)
		if s.err != nil {
			// log.Panic(s.err)
			return nil, http.StatusInternalServerError
		}

	}
	return r[0], http.StatusOK
}

// IsZero retorna si la interface está vacía
func IsZero(x interface{}) bool {
	return reflect.DeepEqual(x, reflect.Zero(reflect.TypeOf(x)).Interface())
}
