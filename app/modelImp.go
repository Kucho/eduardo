package app

// NewModel retona un estructura vacía del tipo de estructura
func (s *Storage) NewModel(mtype string) interface{} {
	var nm interface{}

	switch mtype {
	case "Booking":
		nm = &Booking{}
	case "Cargo":
		nm = &Cargo{}
	case "CargoType":
		nm = &CargoType{}
	case "Client":
		nm = &Client{}
	case "DocType":
		nm = &DocType{}
	case "Company":
		nm = &Company{}
	case "Outgoing":
		nm = &Outgoing{}
	case "Passenger":
		nm = &Passenger{}
	case "Route":
		nm = &Route{}
	case "Staff":
		nm = &Staff{}
	case "Ship":
		nm = &Ship{}
	case "Transaction":
		nm = &Transaction{}
	case "Trip":
		nm = &Trip{}
	}

	return nm
}
