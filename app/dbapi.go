package app

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
)

// Rutas de la api de la base de datos

// Update - actualiza un (01) documento en la base de datos
func (s *Storage) Update(w http.ResponseWriter, r *http.Request) {
	col := strings.Title(chi.URLParam(r, "type"))
	key := chi.URLParam(r, "key")
	v := chi.URLParam(r, "value")

	if checkAPIStruct(col) {

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		defer r.Body.Close()

		msg := s.NewModel(col)
		err = json.Unmarshal(b, &msg)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		var value interface{}
		var c []interface{}

		if !IsZero(v) {
			//Si tenemos un par completo, hacer búsqueda paramétrica
			value, s.err = strconv.Atoi(v)
			if s.err != nil {
				value = v
			}
			c, _ = s.ReadDocs(col, key, value)

		} else {
			// Recupera los documentos de la base de datos según _key
			c, _ = s.ReadDocs(col, "_key", key)
		}

		// Si hay más de un documento o ninguno, mandar mensaje de error y salir
		if len(c) == 0 {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}
		if len(c) > 1 {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		// Lo que envía el cliente
		patch := reflect.ValueOf(msg).Elem()
		// Lo que tenemos en la base de datos
		original := reflect.ValueOf(c[0]).Elem()
		// La versión final de nuestro documento parchado
		patched := reflect.New(original.Type()).Elem()
		for i := 0; i < original.NumField(); i++ {
			// Si el valor el parche no está vacío, usa parche en la versión parchada. De otra forma, usa el original.
			if !IsZero(patch.Field(i).Interface()) {
				// Asigna valor del parche
				patched.Field(i).Set(reflect.Indirect(patch).Field(i))
			} else {
				// Mantiene el valor del original
				patched.Field(i).Set(reflect.Indirect(original).Field(i))
			}
		}

		result, _ := s.UpdateDoc(col, "_key", reflect.Indirect(original).FieldByName("Key").Interface(), patched.Interface())
		output, err := json.Marshal(result)

		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Header().Set("content-type", "application/json")
		w.Write(output)

	} else {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
}

// Delete busca en la base de datos el documento según key, y ejecuta la limpieza
func (s *Storage) Delete(w http.ResponseWriter, r *http.Request) {
	key := chi.URLParam(r, "key")
	col := strings.Title(chi.URLParam(r, "type"))
	v := chi.URLParam(r, "value")

	if checkAPIStruct(col) {

		var value interface{}
		var c []interface{}

		if !IsZero(v) {
			//Si tenemos un par completo, hacer búsqueda paramétrica
			value, s.err = strconv.Atoi(v)
			if s.err != nil {
				value = v
			}
			c, _ = s.ReadDocs(col, key, value)

		} else {
			// Recupera los documentos de la base de datos según _key
			c, _ = s.ReadDocs(col, "_key", key)
		}

		// Solo borraremos uno, para el resto tirar errores
		if len(c) == 0 {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}

		if len(c) > 1 {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		original := reflect.ValueOf(c[0]).Elem()
		result, _ := s.RemoveDoc(col, "_key", reflect.Indirect(original).FieldByName("Key").Interface())
		output, err := json.Marshal(result)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		w.Header().Set("content-type", "application/json")
		w.Write(output)

	} else {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
}

// GetKey es la función de la API que devuelve un documento según el _key de la base de datos, o lista todos los resultados según la colección
func (s *Storage) GetKey(w http.ResponseWriter, r *http.Request) {
	col := strings.Title(chi.URLParam(r, "type"))
	key := chi.URLParam(r, "key")
	v := chi.URLParam(r, "value")

	var value interface{}

	if checkAPIStruct(col) {

		var c []interface{}

		if !IsZero(v) {
			//Si tenemos un par completo, hacer búsqueda paramétrica
			value, s.err = strconv.Atoi(v)
			if s.err != nil {
				value = v
			}
			println(value)
			c, _ = s.ReadDocs(col, key, value)

		} else {
			// Si no tenemos un par completo, buscar solo por key
			c, _ = s.ReadDocs(col, "_key", key)
		}

		if len(c) == 0 {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}

		output, err := json.Marshal(c)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		w.Header().Set("content-type", "application/json")
		w.Write(output)

	} else {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

}

// GetAll es la función de la API que devuelve un documento según el _key de la base de datos, o lista todos los resultados según la colección
func (s *Storage) GetAll(w http.ResponseWriter, r *http.Request) {
	col := strings.Title(chi.URLParam(r, "type"))

	if checkAPIStruct(col) {
		c, _ := s.ReadDocs(col, "", "")

		if len(c) == 0 {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}

		output, err := json.Marshal(c)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		w.Header().Set("content-type", "application/json")
		w.Write(output)

	} else {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

}

// Add es la función de la API que procesa un documento que será agregado a la base de datos
func (s *Storage) Add(w http.ResponseWriter, r *http.Request) {
	col := strings.Title(chi.URLParam(r, "type"))

	if checkAPIStruct(col) {
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		defer r.Body.Close()

		// Lee el body y verificamos que el tipo sea el correcto
		msg := s.NewModel(col)
		err = json.Unmarshal(b, &msg)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		// Encode el mensaje original
		m, _ := json.Marshal(msg)
		// Decodificar para obtener un mapa
		var a interface{}
		json.Unmarshal(m, &a)
		f := a.(map[string]interface{})
		// Eliminar datos que serán generados por la base datos
		delete(f, "_key")
		delete(f, "_id")
		delete(f, "_rev")

		// Retorna el documento agregado
		rd, _ := s.NewDoc(col, f)

		// Convierte el pong con el documento
		output, err := json.Marshal(rd)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Header().Set("content-type", "application/json")
		w.Write(output)
	} else {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
}

// Chequeamos si el método tiene una estructura válida definida por el modelo
func checkAPIStruct(t string) bool {
	valid := false

	for i := 0; i < len(COLLS); i++ {
		if t == COLLS[i] {
			valid = true
			break
		}
	}

	for i := 0; i < len(EDGES); i++ {
		if t == EDGES[i] {
			valid = true
			break
		}
	}
	return valid
}
