package app

// Rutas

import (
	"fmt"
	"net/http"
)

// AdminDB maneja y muestra el dashboard de los administradores
func AdminDB(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf("You are in the dashboard section")))
}

// AdminInfo maneja y muestra la información de la cuenta de un administrador
func AdminInfo(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf("You are in the profile section")))
}
