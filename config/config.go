package config

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"runtime"
	"strings"

	"github.com/spf13/viper"
)

var defaultConf = []byte(`
core:
 enabled: true # enabale httpd server
 address: "" # ip address to bind (default: any)
 port: "8080" # ignore this port number if auto_tls is enabled (listen 443).
 worker_num: 0 # default worker number is runtime.NumCPU()
 queue_num: 0 # default queue number is 8192
 ssl: false
 cert_path: "cert.pem"
 key_path: "key.pem"
 cert_base64: ""
 key_base64: ""
 auto_tls:
  enabled: false # Automatically install TLS certificates from Let's Encrypt.
  folder: ".cache" # folder for storing TLS certificates
  host: "" # which domains the Let's Encrypt will attemptlog:
  format: "string" # string or json
log:
  access_log: "stdout" # stdout: output to console, or define log path like "log/access_log"
  access_level: "debug"
  error_log: "stderr" # stderr: output to console, or define log path like "log/error_log"
  error_level: "error"
  hide_token: true
database:
 arangodb:
  host: "localhost"
  port: "8529"
  user: "root"
  password: "root"
  dbname: "eduardo"`)

// ConfYaml is config structure.
type ConfYaml struct {
	Core     SectionCore     `yaml:"core"`
	Log      SectionLog      `yaml:"log"`
	Database SectionDatabase `yaml:"database"`
}

// SectionCore is sub section of config.
type SectionCore struct {
	Enabled    bool           `yaml:"enabled"`
	Address    string         `yaml:"address"`
	Port       string         `yaml:"port"`
	WorkerNum  int64          `yaml:"worker_num"`
	QueueNum   int64          `yaml:"queue_num"`
	SSL        bool           `yaml:"ssl"`
	CertPath   string         `yaml:"cert_path"`
	KeyPath    string         `yaml:"key_path"`
	CertBase64 string         `yaml:"cert_base64"`
	KeyBase64  string         `yaml:"key_base64"`
	HTTPProxy  string         `yaml:"http_proxy"`
	AutoTLS    SectionAutoTLS `yaml:"auto_tls"`
}

// SectionAutoTLS support Let's Encrypt setting.
type SectionAutoTLS struct {
	Enabled bool   `yaml:"enabled"`
	Folder  string `yaml:"folder"`
	Host    string `yaml:"host"`
}

// SectionLog is sub section of config.
type SectionLog struct {
	Format      string `yaml:"format"`
	AccessLog   string `yaml:"access_log"`
	AccessLevel string `yaml:"access_level"`
	ErrorLog    string `yaml:"error_log"`
	ErrorLevel  string `yaml:"error_level"`
	HideToken   bool   `yaml:"hide_token"`
}

// SectionDatabase is sub section of config.
type SectionDatabase struct {
	ArangoDB SectionArangoDB `yaml:"arangodb"`
}

// SectionArangoDB is sub section of config.
type SectionArangoDB struct {
	Host         string `yaml:"host"`
	Port         string `yaml:"port"`
	User         string `yaml:"user"`
	Password     string `yaml:"password"`
	DatabaseName string `yaml:"dbname"`
}

// LoadConf load config from file and read in environment variables that match
func LoadConf(confPath string) (ConfYaml, error) {
	var conf ConfYaml

	viper.SetConfigType("yaml")
	viper.AutomaticEnv()            // read in environment variables that match
	viper.SetEnvPrefix("goeduardo") // will be uppercased automatically
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if confPath != "" {
		content, err := ioutil.ReadFile(confPath)

		if err != nil {
			return conf, err
		}

		viper.ReadConfig(bytes.NewBuffer(content))

	} else {

		viper.SetConfigName("config")
		viper.AddConfigPath("./config/")
		viper.AddConfigPath(".")

		// If a config file is found, read it in.
		if err := viper.ReadInConfig(); err == nil {
			fmt.Println("Using config file:", viper.ConfigFileUsed())
		} else {
			// load default config
			viper.ReadConfig(bytes.NewBuffer(defaultConf))
		}
	}

	// Core
	conf.Core.Address = viper.GetString("core.address")
	conf.Core.Port = viper.GetString("core.port")
	conf.Core.Enabled = viper.GetBool("core.enabled")
	conf.Core.WorkerNum = int64(viper.GetInt("core.worker_num"))
	conf.Core.QueueNum = int64(viper.GetInt("core.queue_num"))
	conf.Core.SSL = viper.GetBool("core.ssl")
	conf.Core.CertPath = viper.GetString("core.cert_path")
	conf.Core.KeyPath = viper.GetString("core.key_path")
	conf.Core.CertBase64 = viper.GetString("core.cert_base64")
	conf.Core.KeyBase64 = viper.GetString("core.key_base64")
	conf.Core.HTTPProxy = viper.GetString("core.http_proxy")
	conf.Core.AutoTLS.Enabled = viper.GetBool("core.auto_tls.enabled")
	conf.Core.AutoTLS.Folder = viper.GetString("core.auto_tls.folder")
	conf.Core.AutoTLS.Host = viper.GetString("core.auto_tls.host")

	// log
	conf.Log.Format = viper.GetString("log.format")
	conf.Log.AccessLog = viper.GetString("log.access_log")
	conf.Log.AccessLevel = viper.GetString("log.access_level")
	conf.Log.ErrorLog = viper.GetString("log.error_log")
	conf.Log.ErrorLevel = viper.GetString("log.error_level")
	conf.Log.HideToken = viper.GetBool("log.hide_token")

	// database
	conf.Database.ArangoDB.Host = viper.GetString("database.arangodb.host")
	conf.Database.ArangoDB.Port = viper.GetString("database.arangodb.port")
	conf.Database.ArangoDB.User = viper.GetString("database.arangodb.user")
	conf.Database.ArangoDB.Password = viper.GetString("database.arangodb.password")
	conf.Database.ArangoDB.DatabaseName = viper.GetString("database.arangodb.dbname")

	if conf.Core.WorkerNum == int64(0) {
		conf.Core.WorkerNum = int64(runtime.NumCPU())
	}

	if conf.Core.QueueNum == int64(0) {
		conf.Core.QueueNum = int64(8192)
	}

	return conf, nil
}
